module com.example.danovakalkulacka {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens cz.vse.kalkulacka to javafx.fxml;
    exports cz.vse.kalkulacka.logika;
    exports cz.vse.kalkulacka.start;
    opens cz.vse.kalkulacka.start to javafx.fxml;
}
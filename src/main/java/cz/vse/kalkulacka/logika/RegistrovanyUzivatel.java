package cz.vse.kalkulacka.logika;

public class RegistrovanyUzivatel {
    private String xname;
    private String jmeno;
    private String prijmeni;
    private String heslo;
    private IndividualniNastaveni individualniNastaveni;

    public RegistrovanyUzivatel(String xname, String jmeno, String prijmeni, String heslo) {
        this.xname = xname;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.heslo = heslo;
        this.individualniNastaveni = new IndividualniNastaveni();
    }

    public String getXname() {
        return xname;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }

    public IndividualniNastaveni getIndividualniNastaveni() {
        return individualniNastaveni;
    }

    public void setIndividualniNastaveni(IndividualniNastaveni individualniNastaveni) {
        this.individualniNastaveni = individualniNastaveni;
    }

    @Override
    public String toString() {
        return "RegistrovanyUzivatel{" +
                "xname='" + xname + '\'' +
                ", jmeno='" + jmeno + '\'' +
                ", prijmeni='" + prijmeni + '\'' +
                ", heslo='" + heslo + '\'' +
                ", individualniNastaveni=" + individualniNastaveni +
                '}';
    }
}

package cz.vse.kalkulacka.logika;

public class Klient {
    private static Aplikace aplikace;

    public static void inicializovat(Aplikace aplikace) {
        Klient.aplikace = aplikace;
    }

    public static Aplikace getAplikace() {
        return aplikace;
    }


}

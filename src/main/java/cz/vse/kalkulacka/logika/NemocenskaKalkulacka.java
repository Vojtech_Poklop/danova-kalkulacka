package cz.vse.kalkulacka.logika;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
//import java.util.concurrent.TimeUnit;

public class NemocenskaKalkulacka implements IKalkulacka {

    private IndividualniNastaveni individualniNastaveni;
    private int prumMzda4;
    private int prumMzda12;
    private Date zacatekNemoci;
    private Date konecNemoci;


    //pouze technické atributy
    private int pocetPracDni;
    private int pocetVsechDni;
    private double hodinovaNahrada;
    private double nahradaPoSazbe;
    private double hodinovaNemocenska;
    private double nemocenskaPoSazbe;
    private double nemocenska;
    static List<Date> svatky = new ArrayList<>();
    //pouze technické atributy

    //sazby
    private final double ctvrtletniSpodniMaxMzda=256.66;
    private final double ctvrtletniProstredniMaxMzda=384.83;
    private final double ctvrtletniHorniMaxMzda=769.48;

    private final double rocniSpodniMaxMzda=1466;
    private final double rocniProstredniMaxMzda=2199;
    private final double rocniHorniMaxMzda=4397;

    private final double SpodniSazba=0.3;
    private final double ProstredniSazba=0.6;
    private final double HorniSazba=0.9;

    private final double sazba30dni=0.6;
    private final double sazba50dni=0.66;
    private final double sazba61dni=0.72;
    //sazby


    public NemocenskaKalkulacka() {
        this.prumMzda12 = prumMzda12;
        this.prumMzda4 = prumMzda4;
        this.zacatekNemoci = zacatekNemoci;
        this.konecNemoci = konecNemoci;
    }

    @Override
    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele) {
        this.individualniNastaveni = individualniNastaveniUzivatele;

        prumMzda4=individualniNastaveni.getIndPrumMzda4();
        prumMzda12=individualniNastaveni.getIndPrumMzda12();
    }

    @Override
    public void vypocti() {

        spocitejPocetPracDni(getSeznamSvatku());
        pocetPracDni = getpocetPracDni();
        //spocitejPocetVsechDni();
        pocetVsechDni = getPocetVsechDni();

        //náhrada mzdy
        if(prumMzda4<=ctvrtletniSpodniMaxMzda){
            hodinovaNahrada = prumMzda4 * HorniSazba;
        } else if (prumMzda4<=ctvrtletniProstredniMaxMzda) {
            hodinovaNahrada = (ctvrtletniSpodniMaxMzda * HorniSazba) + ((prumMzda4 - ctvrtletniSpodniMaxMzda) * ProstredniSazba);
        } else if (prumMzda4<=ctvrtletniHorniMaxMzda){
            hodinovaNahrada = (ctvrtletniSpodniMaxMzda * HorniSazba) + ((ctvrtletniProstredniMaxMzda-ctvrtletniSpodniMaxMzda) * ProstredniSazba)
            + ((prumMzda4 - ctvrtletniProstredniMaxMzda) * SpodniSazba);
        } else {
            hodinovaNahrada = (ctvrtletniSpodniMaxMzda * HorniSazba) + ((ctvrtletniProstredniMaxMzda-ctvrtletniSpodniMaxMzda) * ProstredniSazba)
            + (ctvrtletniProstredniMaxMzda * SpodniSazba);
        }

        //nemocenská
        if(prumMzda12<=rocniSpodniMaxMzda){
            hodinovaNemocenska = prumMzda12 * HorniSazba;
        } else if (prumMzda12<=rocniProstredniMaxMzda) {
            hodinovaNemocenska = (rocniSpodniMaxMzda * HorniSazba) + ((prumMzda12 - rocniSpodniMaxMzda) * ProstredniSazba);
        } else if (prumMzda12<=rocniHorniMaxMzda){
            hodinovaNemocenska = (rocniSpodniMaxMzda * HorniSazba) + ((rocniProstredniMaxMzda-rocniSpodniMaxMzda) * ProstredniSazba)
            + ((prumMzda12 - rocniProstredniMaxMzda) * SpodniSazba);
        } else {
            hodinovaNemocenska = (rocniSpodniMaxMzda * HorniSazba) + ((rocniProstredniMaxMzda-rocniSpodniMaxMzda) * ProstredniSazba)
            + ((rocniProstredniMaxMzda-1) * SpodniSazba);
        }

        if (pocetVsechDni<=14){
            nahradaPoSazbe = hodinovaNahrada * sazba30dni * 8 * pocetPracDni;
        }else {
            nahradaPoSazbe = hodinovaNahrada * sazba30dni * 8 * 14;
        }

        if (pocetVsechDni>14 && pocetVsechDni<=44){
            nemocenskaPoSazbe = hodinovaNemocenska * sazba30dni * (pocetVsechDni - 14);
        } else if (pocetVsechDni>14 && pocetVsechDni<=74) {
            nemocenskaPoSazbe = (hodinovaNemocenska * sazba30dni * 30) + (hodinovaNemocenska * sazba50dni * pocetVsechDni - 44);
        } else if (pocetVsechDni>=75) {
            nemocenskaPoSazbe = (hodinovaNemocenska * sazba30dni * 30) + (hodinovaNemocenska * sazba50dni * 30)
            + (hodinovaNemocenska * sazba61dni * pocetVsechDni - 74);
        }

        nemocenska = Math.ceil(nahradaPoSazbe + nemocenskaPoSazbe);
    }

    public void spocitejPocetPracDni(List<Date> svatky) {
        getSeznamSvatku();
        Calendar calZacatek = Calendar.getInstance();
        calZacatek.setTime(zacatekNemoci);
        Calendar calKonec = Calendar.getInstance();
        calKonec.setTime(konecNemoci);

        int pocetPracDni = 0;

        while (!calZacatek.after(calKonec)) {
            int dayOfWeek = calZacatek.get(Calendar.DAY_OF_WEEK);

            if ((dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY)) {
                // přeskočení víkendu
            } else if (getSeznamSvatku().contains(calZacatek.getTime())) {
                // přeskočení svátku
            } else {
                pocetPracDni++;
            }

            calZacatek.add(Calendar.DATE, 1);
        }

        setPocetPracDni(this.pocetPracDni = pocetPracDni);
    }
    public static List<Date> getSeznamSvatku() {
        List<Date> svatky = new ArrayList<>();

        svatky.add(getDatum(1, Calendar.JANUARY));

        return svatky;
    }
    private static Date getDatum(int den, int mesic) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, den);
        cal.set(Calendar.MONTH, mesic);
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        return cal.getTime();
    }

    public int getpocetPracDni() {
        return this.pocetPracDni;
    }

    public void setPocetPracDni(int pocetPracDni) {
        this.pocetPracDni = pocetPracDni;
    }

    /*public void spocitejPocetVsechDni() {

        if (zacatekNemoci != null && konecNemoci != null) {

            long rozdilVMillis = konecNemoci.getTime() - zacatekNemoci.getTime();
            long rozdilVDnech = TimeUnit.DAYS.convert(rozdilVMillis, TimeUnit.MILLISECONDS);
            
            setPocetVsechDni(pocetVsechDni = (int) rozdilVDnech);
        } else {
            throw new IllegalArgumentException("Datum nevyplněn");
        }
    }
*/
    public int getPocetVsechDni() {
        return pocetVsechDni;
    }

    public void setPocetVsechDni(int pocetVsechDni) {
        this.pocetVsechDni = pocetVsechDni;
    }
}

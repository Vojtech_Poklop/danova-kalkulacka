package cz.vse.kalkulacka.logika;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.sql.SQLException;
import java.sql.ResultSet;

public class Aplikace {
    private IKalkulacka kalkulacka;
    private RegistrovanyUzivatel registrovanyUzivatel;
    private boolean jePrihlasen;
    private final DatabaseConnection databazeHandler;
    private IndividualniNastaveni defaultniIndividualniNastaveni;

    public Aplikace() {
        this.registrovanyUzivatel = null;
        this.jePrihlasen = false;
        this.kalkulacka = null;
        this.databazeHandler = new DatabaseConnection();
        this.defaultniIndividualniNastaveni = new IndividualniNastaveni();
        pripojitKDatabazi();
    }

    private void pripojitKDatabazi(){
        databazeHandler.getConnection();
        System.out.println("Připojeno");
    }

    private void odpojitOdDatabaze(){
       databazeHandler.closeConnection();
    }

    public void odhlasitSe(){
        this.registrovanyUzivatel = null;
        this.jePrihlasen = false;
        System.out.println("Uživatel odhlášen");
    }

    public boolean registrovatSe(String vlozeneXname, String vlozeneJmeno, String vlozenePrijmeni, String vlozeneHeslo){
        if(vlozeneXname.equals("") || vlozeneJmeno.equals("") || vlozenePrijmeni.equals("") || vlozeneHeslo.equals("")){
            return false;
        }

        try {
            // Kontrola, zda uživatel s daným xname již existuje
            String kontrolniQuery = "SELECT * FROM UZIVATEL WHERE XNAME = ?";
            try (PreparedStatement kontrolniStatement = databazeHandler.getConnection().prepareStatement(kontrolniQuery)) {
                kontrolniStatement.setString(1, vlozeneXname);
                try (ResultSet kontrolniResultSet = kontrolniStatement.executeQuery()) {
                    if (kontrolniResultSet.next()) {
                        System.out.println("Registrace selhala: Uživatel s daným XNAME již existuje.");
                        return false; // Uživatel s daným XNAME již existuje, nelze registrovat
                    }
                }
            }

            // Vložení nového uživatele do databáze
            String insertQuery = "INSERT INTO UZIVATEL (XNAME, JMENO, PRIJMENI, HESLO) VALUES (?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = databazeHandler.getConnection().prepareStatement(insertQuery)) {
                preparedStatement.setString(1, vlozeneXname);
                preparedStatement.setString(2, vlozeneJmeno);
                preparedStatement.setString(3, vlozenePrijmeni);
                preparedStatement.setString(4, vlozeneHeslo);

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 1) {
                    registrovanyUzivatel = new RegistrovanyUzivatel(vlozeneXname, vlozeneJmeno,vlozenePrijmeni,vlozeneHeslo);
                    this.jePrihlasen = true;
                    ulozNastaveniDoDatabaze(vlozeneXname,registrovanyUzivatel.getIndividualniNastaveni());
                    System.out.println("Registrace byla úspěšná");
                    return true; // Registrace byla úspěšná
                } else {
                    System.out.println("Registrace selhala: Neznámá chyba při vkládání do databáze.");
                    return false; // Registrace selhala
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false; // Něco se pokazilo
        }
    }

    public boolean prihlasitSe(String xname, String heslo) {
        try {
            String query = "SELECT * FROM UZIVATEL WHERE XNAME = ? AND HESLO = ?";
            try (PreparedStatement preparedStatement = databazeHandler.getConnection().prepareStatement(query)) {
                preparedStatement.setString(1, xname);
                preparedStatement.setString(2, heslo);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        String jmeno = resultSet.getString("JMENO");
                        String prijmeni = resultSet.getString("PRIJMENI");
                        registrovanyUzivatel = new RegistrovanyUzivatel(xname, jmeno, prijmeni, heslo);

                        nactiIndividualniNastaveni(xname);

                        this.jePrihlasen = true;
                        System.out.println("Přihlášení bylo úspěšné");
                    } else {
                        System.out.println("Přihlášení selhalo: neplatný XNAME nebo HESLO.");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jePrihlasen;
    }

    public void nactiIndividualniNastaveni(String xname) {
        // Načtení individuálního nastavení z databáze
        String nacteniNastaveniQuery = "SELECT * FROM INDIVIDUALNINASTAVENI WHERE XNAME = ?";
        try (PreparedStatement nacteniStatement = databazeHandler.getConnection().prepareStatement(nacteniNastaveniQuery)) {
            nacteniStatement.setString(1, xname);

            try (ResultSet nastaveniResultSet = nacteniStatement.executeQuery()) {
                if (nastaveniResultSet.next()) {
                    IndividualniNastaveni nastaveni = new IndividualniNastaveni(
                            nastaveniResultSet.getInt("HRUBAMZDA"),
                            nastaveniResultSet.getInt("POCETDETI"),
                            nastaveniResultSet.getInt("CENASLUZEBNIHOVOZU"),
                            nastaveniResultSet.getString("TYPAUTO"),
                            nastaveniResultSet.getBoolean("INVALIDITA12"),
                            nastaveniResultSet.getBoolean("INVALIDITA3"),
                            nastaveniResultSet.getBoolean("PRUKAZZTP"),
                            nastaveniResultSet.getInt("PRIJMY"),
                            nastaveniResultSet.getInt("VYDAJE"),
                            nastaveniResultSet.getDouble("PAUSAL"),
                            nastaveniResultSet.getBoolean("MANZEL"),
                            nastaveniResultSet.getInt("PENZIJNIPOJ"),
                            nastaveniResultSet.getInt("ZIVOTPOJ"),
                            nastaveniResultSet.getInt("ODPOCETUROKU"),
                            nastaveniResultSet.getInt("SKOLKOVNE"),
                            nastaveniResultSet.getInt("PRUMMZDA4"),
                            nastaveniResultSet.getInt("PRUMMZDA12")
                    );

                    registrovanyUzivatel.setIndividualniNastaveni(nastaveni);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void ulozNastaveniDoDatabaze(String xname, IndividualniNastaveni individualniNastaveni) {
        try {
            // Kontrola, zda záznam s daným xname již existuje
            String kontrolniQuery = "SELECT * FROM INDIVIDUALNINASTAVENI WHERE XNAME = ?";
            try (PreparedStatement kontrolniStatement = databazeHandler.getConnection().prepareStatement(kontrolniQuery)) {
                kontrolniStatement.setString(1, xname);
                try (ResultSet kontrolniResultSet = kontrolniStatement.executeQuery()) {
                    if (!kontrolniResultSet.next()) {
                        // Záznam s daným xname neexistuje, vytvořit nový záznam
                        vytvorNovyZaznam(xname, individualniNastaveni);
                        return;
                    }
                }
            }

            // Aktualizace hodnot v tabulce INDIVIDUALNINASTAVENI pro daného uživatele (XNAME)
            String updateQuery = "UPDATE INDIVIDUALNINASTAVENI SET " +
                    "HRUBAMZDA = ?, POCETDETI = ?, CENASLUZEBNIHOVOZU = ?, TYPAUTO = ?, " +
                    "INVALIDITA12 = ?, INVALIDITA3 = ?, PRUKAZZTP = ?, PRIJMY = ?, VYDAJE = ?, " +
                    "PAUSAL = ?, MANZEL = ?, PENZIJNIPOJ = ?, ZIVOTPOJ = ?, " +
                    "ODPOCETUROKU = ?, SKOLKOVNE = ?, PRUMMZDA4 = ?, PRUMMZDA12 = ? " +
                    "WHERE XNAME = ?";

            try (PreparedStatement updateStatement = databazeHandler.getConnection().prepareStatement(updateQuery)) {
                updateStatement.setInt(1, individualniNastaveni.getIndHrubaMzda());
                updateStatement.setInt(2, individualniNastaveni.getIndPocetDeti());
                updateStatement.setInt(3, individualniNastaveni.getIndCenaSluzebnihoVozu());
                updateStatement.setString(4, individualniNastaveni.getIndTypAuto());
                updateStatement.setBoolean(5, individualniNastaveni.isIndInvalidita12());
                updateStatement.setBoolean(6, individualniNastaveni.isIndInvalidita3());
                updateStatement.setBoolean(7, individualniNastaveni.isIndPrukazZTP());
                updateStatement.setInt(8, individualniNastaveni.getIndPrijmy());
                updateStatement.setInt(9, individualniNastaveni.getIndVydaje());
                updateStatement.setDouble(10, individualniNastaveni.getIndPausal());
                updateStatement.setBoolean(11, individualniNastaveni.isIndManzel());
                updateStatement.setInt(12, individualniNastaveni.getIndPenzijniPoj());
                updateStatement.setInt(13, individualniNastaveni.getIndZivotPoj());
                updateStatement.setInt(14, individualniNastaveni.getIndOdpocetUroku());
                updateStatement.setInt(15, individualniNastaveni.getIndSkolkovne());
                updateStatement.setInt(16, individualniNastaveni.getIndPrumMzda4());
                updateStatement.setInt(17, individualniNastaveni.getIndPrumMzda12());
                updateStatement.setString(18, xname);

                int affectedRows = updateStatement.executeUpdate();

                if (affectedRows > 0) {
                    System.out.println("Aktualizace nastavení byla úspěšná");
                } else {
                    System.out.println(xname);
                    System.out.println("Aktualizace nastavení selhala: Neznámá chyba při aktualizaci v databázi.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void vytvorNovyZaznam(String xname, IndividualniNastaveni individualniNastaveni) {
        try {
            // Vytvoření nového záznamu v tabulce INDIVIDUALNI_NASTAVENI pro daného uživatele (XNAME)
            String insertQuery = "INSERT INTO INDIVIDUALNINASTAVENI (XNAME, HRUBAMZDA, POCETDETI, CENASLUZEBNIHOVOZU, TYPAUTO, " +
                    "INVALIDITA12, INVALIDITA3, PRUKAZZTP, PRIJMY, VYDAJE, PAUSAL, MANZEL, PENZIJNIPOJ, ZIVOTPOJ, " +
                    "ODPOCETUROKU, SKOLKOVNE, PRUMMZDA4, PRUMMZDA12) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            try (PreparedStatement insertStatement = databazeHandler.getConnection().prepareStatement(insertQuery)) {
                insertStatement.setString(1, xname);
                insertStatement.setInt(2, individualniNastaveni.getIndHrubaMzda());
                insertStatement.setInt(3, individualniNastaveni.getIndPocetDeti());
                insertStatement.setInt(4, individualniNastaveni.getIndCenaSluzebnihoVozu());
                insertStatement.setString(5, individualniNastaveni.getIndTypAuto());
                insertStatement.setBoolean(6, individualniNastaveni.isIndInvalidita12());
                insertStatement.setBoolean(7, individualniNastaveni.isIndInvalidita3());
                insertStatement.setBoolean(8, individualniNastaveni.isIndPrukazZTP());
                insertStatement.setInt(9, individualniNastaveni.getIndPrijmy());
                insertStatement.setInt(10, individualniNastaveni.getIndVydaje());
                insertStatement.setDouble(11, individualniNastaveni.getIndPausal());
                insertStatement.setBoolean(12, individualniNastaveni.isIndManzel());
                insertStatement.setInt(13, individualniNastaveni.getIndPenzijniPoj());
                insertStatement.setInt(14, individualniNastaveni.getIndZivotPoj());
                insertStatement.setInt(15, individualniNastaveni.getIndOdpocetUroku());
                insertStatement.setInt(16, individualniNastaveni.getIndSkolkovne());
                insertStatement.setInt(17, individualniNastaveni.getIndPrumMzda4());
                insertStatement.setInt(18, individualniNastaveni.getIndPrumMzda12());

                int affectedRows = insertStatement.executeUpdate();

                if (affectedRows > 0) {
                    System.out.println("Vytvoření nového záznamu bylo úspěšné");
                } else {
                    System.out.println("Vytvoření nového záznamu selhalo: Neznámá chyba při vkládání do databáze.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }




    public boolean zmenaHesla(String stareHeslo, String noveHeslo){
        if(registrovanyUzivatel != null){
            if(this.registrovanyUzivatel.getHeslo().equals(stareHeslo)){
                try {
                    String updateQuery = "UPDATE UZIVATEL SET HESLO = ? WHERE XNAME = ?";
                    try (PreparedStatement preparedStatement = databazeHandler.getConnection().prepareStatement(updateQuery)){
                        preparedStatement.setString(1, noveHeslo);
                        preparedStatement.setString(2,this.registrovanyUzivatel.getXname());

                        int affectedRows = preparedStatement.executeUpdate();

                        if(affectedRows == 1){
                            System.out.println("Změna hesla byla úspěšná");
                            this.registrovanyUzivatel.setHeslo(noveHeslo);
                            return true;
                        } else {
                            System.out.println("Změna hesla selhala: Neznámá chyba při aktualizaci v databázi.");
                        }
                    }
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean isJePrihlasen() {
        return jePrihlasen;
    }

    public void otevriKalkulacku(IKalkulacka novaKalkulacka){
//        if(kalkulacka != null){
            this.kalkulacka = novaKalkulacka;
            if(registrovanyUzivatel != null){
                this.kalkulacka.nactiIndividualniAtributy(registrovanyUzivatel.getIndividualniNastaveni());
            } else {
                this.kalkulacka.nactiIndividualniAtributy(defaultniIndividualniNastaveni);
            }
//        }
    }

    public RegistrovanyUzivatel getRegistrovanyUzivatel() {
        return registrovanyUzivatel;
    }

    public IKalkulacka getKalkulacka() {
        return kalkulacka;
    }

    public boolean zmenaJmenaPrijmeniUzivatele(String noveJmeno, String novePrijmeni){
        if(registrovanyUzivatel != null && (!noveJmeno.equals("") || !novePrijmeni.equals(""))){
            // Kontrola, zda došlo ke skutečné změně
            if (!noveJmeno.equals(registrovanyUzivatel.getJmeno()) || !novePrijmeni.equals(registrovanyUzivatel.getPrijmeni())) {
                try {
                    String updateQuery = "UPDATE UZIVATEL SET JMENO = ?, PRIJMENI = ? WHERE XNAME = ?";
                    try (PreparedStatement preparedStatement = databazeHandler.getConnection().prepareStatement(updateQuery)){
                        preparedStatement.setString(1, noveJmeno);
                        preparedStatement.setString(2, novePrijmeni);
                        preparedStatement.setString(3, this.registrovanyUzivatel.getXname());

                        int affectedRows = preparedStatement.executeUpdate();

                        if(affectedRows == 1){
                            System.out.println("Změna jména a příjmení byla úspěšná");
                            this.registrovanyUzivatel.setJmeno(noveJmeno);
                            this.registrovanyUzivatel.setPrijmeni(novePrijmeni);
                            return true;
                        } else {
                            System.out.println("Změna jména a příjmení selhala: Neznámá chyba při aktualizaci v databázi.");
                        }
                    }
                } catch (SQLException e){
                    e.printStackTrace();
                }
            } else {
                System.out.println("Změna jména a příjmení není nutná, nebyly provedeny žádné změny.");
            }
        }
        return false;
    }

    public IndividualniNastaveni getDefaultniIndividualniNastaveni() {
        return defaultniIndividualniNastaveni;
    }
}

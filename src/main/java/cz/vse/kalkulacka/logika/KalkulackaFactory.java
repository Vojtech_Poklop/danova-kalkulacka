package cz.vse.kalkulacka.logika;

public class KalkulackaFactory {
    public IKalkulacka vytvorKalkulacku(String typKalkulacky){
        if (typKalkulacky.equals("DanovaKalkulacka")){
            return new DanovaKalkulacka();
        } else if (typKalkulacky.equals("MzdovaKalkulacka")) {
            return new MzdovaKalkulacka();
        } else if (typKalkulacky.equals("SporeniKalkulacka")) {
            return new SporeniKalkulacka();
        } else if (typKalkulacky.equals("VydajovaKalkulacka")) {
            return new VydajovaKalkulacka();
        } else if (typKalkulacky.equals("NemocenskaKalkulacka")) {
            return new NemocenskaKalkulacka();
        } else if (typKalkulacky.equals("HypotecniKalkulacka")) {
            return new HypotecniKalkulacka();
        } else {
            return null;
        }
    }
}

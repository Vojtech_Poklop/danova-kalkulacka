package cz.vse.kalkulacka.logika;

import java.util.ArrayList;
import java.util.List;

public class SporeniKalkulacka implements IKalkulacka {

    private IndividualniNastaveni individualniNastaveni;
    private int pocatecniVklad;
    private int mesicniUlozka;
    private double rocniUrokSporeni;
    private int dobaSporeni;
    private double danZUroku;
    private List<String> vystupyRok = new ArrayList<>();
    private double posledniNasporenaCastka;
    private double posledniZTohoUrok;

    public SporeniKalkulacka() {
        this.pocatecniVklad = pocatecniVklad;
        this.mesicniUlozka = mesicniUlozka;
        this.rocniUrokSporeni = rocniUrokSporeni;
        this.dobaSporeni = dobaSporeni;
        this.danZUroku = danZUroku;
        this.vystupyRok = new ArrayList<>();
    }

    @Override
    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele) {
        this.individualniNastaveni = individualniNastaveniUzivatele;

    }

    @Override
    public void vypocti() {
        double nasporenaCastka = 0;
        double zTohoUrok = 0;
        
        for(int rok = 1; rok <= dobaSporeni; rok++){
            double urok = mesicniUlozka * 12 * rocniUrokSporeni;
            double dan = urok * danZUroku;

            if (rok == 1){
                nasporenaCastka += pocatecniVklad + mesicniUlozka * 12 + urok - dan;
            }else {
                nasporenaCastka += mesicniUlozka * 12 + urok - dan;
            }
            zTohoUrok += urok;

            String vystupRok = String.format("%d, %d",nasporenaCastka, zTohoUrok);
            vystupyRok.add(vystupRok);

            posledniNasporenaCastka = nasporenaCastka;
            posledniZTohoUrok = zTohoUrok;
        }
    }

    public double getPosledniNasporenaCastka() {
        return posledniNasporenaCastka;
    }

    public double getPosledniZTohoUrokUrok() {
        return posledniZTohoUrok;
    }
}

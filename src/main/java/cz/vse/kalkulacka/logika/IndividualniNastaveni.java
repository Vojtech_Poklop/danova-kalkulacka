package cz.vse.kalkulacka.logika;

public class IndividualniNastaveni {

    private int indHrubaMzda;
    private int indPocetDeti;
    private int indCenaSluzebnihoVozu;
    private String indTypAuto;
    private boolean indInvalidita12;
    private boolean indInvalidita3;
    private boolean indPrukazZTP;
    private int indPrijmy;
    private int indVydaje;
    private double indPausal;
    private boolean indManzel;
    private int indPenzijniPoj;
    private int indZivotPoj;
    private int indOdpocetUroku;
    private int indSkolkovne;
    private int indPrumMzda4;
    private int indPrumMzda12;

    public IndividualniNastaveni() {
        this.indHrubaMzda = 0;
        this.indPocetDeti = 0;
        this.indCenaSluzebnihoVozu = 0;
        this.indTypAuto = "";
        this.indInvalidita12 = false;
        this.indInvalidita3 = false;
        this.indPrukazZTP = false;
        this.indPrijmy = 0;
        this.indVydaje = 0;
        this.indPausal = 0;
        this.indManzel = false;
        this.indPenzijniPoj = 0;
        this.indZivotPoj = 0;
        this.indOdpocetUroku = 0;
        this.indSkolkovne = 0;
        this.indPrumMzda4 = 0;
        this.indPrumMzda12 = 0;
    }

    public IndividualniNastaveni(int indHrubaMzda, int indPocetDeti, int indCenaSluzebnihoVozu, String indTypAuto, boolean indInvalidita12, boolean indInvalidita3, boolean indPrukazZTP, int indPrijmy, int indVydaje, double indPausal, boolean indManzel, int indPenzijniPoj, int indZivotPoj, int indOdpocetUroku, int indSkolkovne, int indPrumMzda4, int indPrumMzda12) {
        this.indHrubaMzda = indHrubaMzda;
        this.indPocetDeti = indPocetDeti;
        this.indCenaSluzebnihoVozu = indCenaSluzebnihoVozu;
        this.indTypAuto = indTypAuto;
        this.indInvalidita12 = indInvalidita12;
        this.indInvalidita3 = indInvalidita3;
        this.indPrukazZTP = indPrukazZTP;
        this.indPrijmy = indPrijmy;
        this.indVydaje = indVydaje;
        this.indPausal = indPausal;
        this.indManzel = indManzel;
        this.indPenzijniPoj = indPenzijniPoj;
        this.indZivotPoj = indZivotPoj;
        this.indOdpocetUroku = indOdpocetUroku;
        this.indSkolkovne = indSkolkovne;
        this.indPrumMzda4 = indPrumMzda4;
        this.indPrumMzda12 = indPrumMzda12;
    }

    public int getIndHrubaMzda() {
        return indHrubaMzda;
    }

    public void setIndHrubaMzda(int indHrubaMzda) {
        this.indHrubaMzda = indHrubaMzda;
    }

    public int getIndPocetDeti() {
        return indPocetDeti;
    }

    public void setIndPocetDeti(int indPocetDeti) {
        this.indPocetDeti = indPocetDeti;
    }

    public int getIndCenaSluzebnihoVozu() {
        return indCenaSluzebnihoVozu;
    }

    public void setIndCenaSluzebnihoVozu(int indCenaSluzebnihoVozu) {
        this.indCenaSluzebnihoVozu = indCenaSluzebnihoVozu;
    }

    public String getIndTypAuto() {
        return indTypAuto;
    }

    public void setIndTypAuto(String indTypAuto) {
        this.indTypAuto = indTypAuto;
    }

    public boolean isIndInvalidita12() {
        return indInvalidita12;
    }

    public void setIndInvalidita12(boolean indInvalidita12) {
        this.indInvalidita12 = indInvalidita12;
    }

    public boolean isIndInvalidita3() {
        return indInvalidita3;
    }

    public void setIndInvalidita3(boolean indInvalidita3) {
        this.indInvalidita3 = indInvalidita3;
    }

    public boolean isIndPrukazZTP() {
        return indPrukazZTP;
    }

    public void setIndPrukazZTP(boolean indPrukazZTP) {
        this.indPrukazZTP = indPrukazZTP;
    }

    public int getIndPrijmy() {
        return indPrijmy;
    }

    public void setIndPrijmy(int indPrijmy) {
        this.indPrijmy = indPrijmy;
    }

    public int getIndVydaje() {
        return indVydaje;
    }

    public void setIndVydaje(int indVydaje) {
        this.indVydaje = indVydaje;
    }

    public double getIndPausal() {
        return indPausal;
    }

    public void setIndPausal(double indPausal) {
        this.indPausal = indPausal;
    }

    public boolean isIndManzel() {
        return indManzel;
    }

    public void setIndManzel(boolean indManzel) {
        this.indManzel = indManzel;
    }

    public int getIndPenzijniPoj() {
        return indPenzijniPoj;
    }

    public void setIndPenzijniPoj(int indPenzijniPoj) {
        this.indPenzijniPoj = indPenzijniPoj;
    }

    public int getIndZivotPoj() {
        return indZivotPoj;
    }

    public void setIndZivotPoj(int indZivotPoj) {
        this.indZivotPoj = indZivotPoj;
    }

    public int getIndOdpocetUroku() {
        return indOdpocetUroku;
    }

    public void setIndOdpocetUroku(int indOdpocetUroku) {
        this.indOdpocetUroku = indOdpocetUroku;
    }

    public int getIndSkolkovne() {
        return indSkolkovne;
    }

    public void setIndSkolkovne(int indSkolkovne) {
        this.indSkolkovne = indSkolkovne;
    }

    public int getIndPrumMzda4() {
        return indPrumMzda4;
    }

    public void setIndPrumMzda4(int indPrumMzda4) {
        this.indPrumMzda4 = indPrumMzda4;
    }

    public int getIndPrumMzda12() {
        return indPrumMzda12;
    }

    public void setIndPrumMzda12(int indPrumMzda12) {
        this.indPrumMzda12 = indPrumMzda12;
    }


    @Override
    public String toString() {
        return "IndividualniNastaveni{" +
                "indHrubaMzda=" + indHrubaMzda +
                ", indPocetDeti=" + indPocetDeti +
                ", indCenaSluzebnihoVozu=" + indCenaSluzebnihoVozu +
                ", indTypAuto='" + indTypAuto + '\'' +
                ", indInvalidita12=" + indInvalidita12 +
                ", indInvalidita3=" + indInvalidita3 +
                ", indPrukazZTP=" + indPrukazZTP +
                ", indPrijmy=" + indPrijmy +
                ", indVydaje=" + indVydaje +
                ", indPausal=" + indPausal +
                ", indManzel=" + indManzel +
                ", indPenzijniPoj=" + indPenzijniPoj +
                ", indZivotPoj=" + indZivotPoj +
                ", indOdpocetUroku=" + indOdpocetUroku +
                ", indSkolkovne=" + indSkolkovne +
                ", indPrumMzda4=" + indPrumMzda4 +
                ", indPrumMzda12=" + indPrumMzda12 +
                '}';
    }
}

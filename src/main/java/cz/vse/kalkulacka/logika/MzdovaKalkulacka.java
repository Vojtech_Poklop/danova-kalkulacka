package cz.vse.kalkulacka.logika;

public class MzdovaKalkulacka implements IKalkulacka {

    private IndividualniNastaveni individualniNastaveni;
    private int hrubaMzda;
    private int pocetDeti;
    private int cenaSluzebnihoVozu;
    private boolean invalidita12;
    private boolean invalidita3;
    private boolean prukazZTP;


    //pouze technické atributy

    private double zustatekPoInvalidech;
    private double zustatekPoZTP;
    //private double zvyhodneniDeti;
    //private double danZvyhodneni;

    private String typAuto;
    //pouze technické atributy



    private double cistaMzda;
    private double ZD;
    private double danPredSlevami;
    private double slevyNaDani;
    private double dan;
    private double zvyhodneniDeti;
    private double danovyBonusCelkem;
    private double socZdravZamestnanec;
    private double socZdravZamestnavatel;
    private double odvody;


    //sazby
    private final double sazbaAutoBezemisni = 0.001;
    private final double sazbaAutoNizkoEmisni = 0.005;
    private final double sazbaAutoBezne = 0.0025;
    private final int maxVyseAuto = 1000;
    private final double sazbaSocZdravZamestnanec = 0.11;
    private final double sazbaSocZdravZamestnavatel = 0.338;
    private final double sazbaNemocenska = 0.006;
    private final double sazbaDan = 0.15;
    private final int maxVysePoplatnik = 2570;
    private final int maxVyseInvalidita12 = 210;
    private final int maxVyseInvalidita3 = 420;
    private final int maxVyseZTP = 1345;
    private final int dite1 = 1267;
    private final int dite2 = 3127;
    private final int diteDalsi = 2320;
    private final int poplatnik = 2570;
    //sazby


    public MzdovaKalkulacka() {
        this.hrubaMzda = 0;
        this.pocetDeti = 0;
        this.cenaSluzebnihoVozu = 0;
        this.invalidita12 = false;
        this.invalidita3 = false;
        this.prukazZTP = false;
        this.typAuto = "Běžné";
    }

    @Override
    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele) {
        this.individualniNastaveni = individualniNastaveniUzivatele;
        hrubaMzda = individualniNastaveni.getIndHrubaMzda();
        pocetDeti = individualniNastaveni.getIndPocetDeti();
        cenaSluzebnihoVozu = individualniNastaveni.getIndCenaSluzebnihoVozu();
        invalidita12 = individualniNastaveni.isIndInvalidita12();
        invalidita3 = individualniNastaveni.isIndInvalidita3();
        prukazZTP = individualniNastaveni.isIndPrukazZTP();
        typAuto = individualniNastaveni.getIndTypAuto();
    }

    @Override
    public void vypocti() {

        double sazbaAuto;
        if (typAuto.equals("Běžné")){
            sazbaAuto=sazbaAutoBezne;
        }else if(typAuto.equals("Nízkoemisní")){
            sazbaAuto=sazbaAutoNizkoEmisni;
        }else if(typAuto.equals("Bezemisní")){
            sazbaAuto=sazbaAutoBezemisni;
        }else {
            sazbaAuto=0;
            System.out.println("Není vyplněn typ auta");
        }

        if(cenaSluzebnihoVozu != 0) {
            if (cenaSluzebnihoVozu * sazbaAuto > maxVyseAuto) {
                ZD = hrubaMzda + cenaSluzebnihoVozu * sazbaAuto;
            } else {
                ZD = hrubaMzda + maxVyseAuto;
            }
        } else {
            ZD = hrubaMzda;
        }


        if (invalidita12 == true) {
            zustatekPoInvalidech = maxVyseInvalidita12;
        } else if (invalidita3 == true) {
            zustatekPoInvalidech = maxVyseInvalidita3;
        } else {
            zustatekPoInvalidech = 0;
        }

        if (prukazZTP == true) {
            zustatekPoZTP = maxVyseZTP;
        } else {
            zustatekPoZTP = 0;
        }

        danPredSlevami = ZD * sazbaDan;
        slevyNaDani = poplatnik + zustatekPoInvalidech + zustatekPoZTP;

        if (danPredSlevami-slevyNaDani < 0){
            dan = 0;
        }else {
            dan = danPredSlevami-slevyNaDani;
        }

        if(pocetDeti == 0){
            zvyhodneniDeti = 0;
        } else if (pocetDeti == 1) {
            zvyhodneniDeti = dite1;
        } else if (pocetDeti == 2) {
            zvyhodneniDeti = dite2;
        } else if (pocetDeti >2) {
            zvyhodneniDeti = dite2 + (pocetDeti - 2)*diteDalsi;
        }

        danovyBonusCelkem=zvyhodneniDeti;
        socZdravZamestnanec=ZD*sazbaSocZdravZamestnanec+ZD*sazbaNemocenska;
        socZdravZamestnavatel=ZD*sazbaSocZdravZamestnavatel;
        cistaMzda=hrubaMzda-dan-socZdravZamestnanec+danovyBonusCelkem;
        odvody=-dan-socZdravZamestnanec-socZdravZamestnavatel+danovyBonusCelkem;
    }

    public IndividualniNastaveni getIndividualniNastaveni() {
        return individualniNastaveni;
    }

    public void setHrubaMzda(int hrubaMzda) {
        this.hrubaMzda = hrubaMzda;
    }

    public void setPocetDeti(int pocetDeti) {
        this.pocetDeti = pocetDeti;
    }

    public void setCenaSluzebnihoVozu(int cenaSluzebnihoVozu) {
        this.cenaSluzebnihoVozu = cenaSluzebnihoVozu;
    }

    public void setInvalidita12(boolean invalidita12) {
        this.invalidita12 = invalidita12;
    }

    public void setInvalidita3(boolean invalidita3) {
        this.invalidita3 = invalidita3;
    }

    public void setPrukazZTP(boolean prukazZTP) {
        this.prukazZTP = prukazZTP;
    }

    public void setTypAuto(String typAuto) {
        this.typAuto = typAuto;
    }




    public double getCistaMzda() {
        return cistaMzda;
    }
    public double getZD() {
        return ZD;
    }

    public double getDanPredSlevami() {
        return danPredSlevami;
    }

    public double getSlevyNaDani() {
        return slevyNaDani;
    }

    public double getDan() {
        return dan;
    }

    public double getZvyhodneniDeti() {
        return zvyhodneniDeti;
    }

    public double getDanovyBonusCelkem() {
        return danovyBonusCelkem;
    }

    public double getSocZdravZamestnanec() {
        return socZdravZamestnanec;
    }

    public double getSocZdravZamestnavatel() {
        return socZdravZamestnavatel;
    }

    public double getOdvody() {
        return odvody;
    }
}

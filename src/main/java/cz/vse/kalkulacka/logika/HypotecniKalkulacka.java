package cz.vse.kalkulacka.logika;

import java.util.ArrayList;
import java.util.List;

public class HypotecniKalkulacka implements IKalkulacka {

    private IndividualniNastaveni individualniNastaveni;
    private int hypotecniPujcka;
    private double urokRokovaSazba;
    private int dobaSplaceniVLetech;
    private List<String> vystupyRok = new ArrayList<>();
    private List<String> vystupyMesic = new ArrayList<>();

    public HypotecniKalkulacka() {
        this.hypotecniPujcka = hypotecniPujcka;
        this.urokRokovaSazba = urokRokovaSazba;
        this.dobaSplaceniVLetech = dobaSplaceniVLetech;
        this.vystupyRok = new ArrayList<>();
        this.vystupyMesic = new ArrayList<>();
    }

    @Override
    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele) {
        this.individualniNastaveni = individualniNastaveniUzivatele;
    }

    @Override
    public void vypocti() {
        double urokRokCelkem = 0;
        double zustatekHypoteky = hypotecniPujcka;

        //výpočty pro každý rok
        for (int rok = 1; rok <= dobaSplaceniVLetech; rok++) {
            double urokRok = zustatekHypoteky * (urokRokovaSazba / 100);
            double mesicniSplatka = hypotecniPujcka / (dobaSplaceniVLetech * 12);
            double zustatekPoSplate = zustatekHypoteky - (mesicniSplatka - urokRok);

            urokRokCelkem += urokRok;
            zustatekHypoteky = zustatekPoSplate;

            String vystupRok = String.format("Rok %d: Zůstatek = %.2f, Úrok celkem = %.2f", rok, zustatekHypoteky, urokRokCelkem);
            vystupyRok.add(vystupRok);
        }

        //výpočty pro každý měsíc
        for (int rok = 1; rok <= dobaSplaceniVLetech*12; rok++){
            double urokMesic = zustatekHypoteky * (urokRokovaSazba / 100) / 12;
            double mesicniSplatka = hypotecniPujcka / (dobaSplaceniVLetech * 12);
            double zustatekPoSplate = zustatekHypoteky - (mesicniSplatka - urokMesic);

            urokRokCelkem += urokMesic;
            zustatekHypoteky = zustatekPoSplate;

            String vystupMesic = String.format("%d,%.2f,%.2f", rok, zustatekHypoteky, urokRokCelkem);
            vystupyMesic.add(vystupMesic);
        }
    }


    public List<String> ziskejVystupyRok() {
        return vystupyRok;
    }

    public List<String> ziskejVystupyMesic(){
        return vystupyMesic;
    }
}

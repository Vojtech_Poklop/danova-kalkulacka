package cz.vse.kalkulacka.logika;

public class DanovaKalkulacka implements IKalkulacka {

    private IndividualniNastaveni individualniNastaveni;
    private boolean isVydajove;
    private int prijmy;
    private int vydaje;
    private double pausal;
    private int pocetDeti;
    private int penzijniPoj;
    private int zivotPoj;
    private int odpocetUroku;
    private boolean poplatnik;
    private boolean invalidita12;
    private boolean invalidita3;
    private boolean prukazZTP;
    private boolean manzel;
    private boolean student;
    private int skolkovne;


    //pouze technické atributy
    private double ZD;
    private double soc;
    private double zdrav;
    private double danSleva;
    private double danPoSleva;
    private double danBonus;
    private double dan;
    //pouze technické atributy

    //sazby
    private final int celkoveSocialni=120972;
    private final int maxVyseSocialni=35324;
    private final double sazbaSocialni=0.292;
    private final int celkoveZdravotni=241944;
    private final int maxVyseZdravotni=32663;
    private final double sazbaZdravotni=0.135;
    private final int maxVysePenzijni=24000;
    private final int minVysePenzijni=12000;
    private final int maxVyseZivotni=24000;
    private final int minVyseZivotni=12000;
    private final int maxOdpocetUroku=150000;
    private final int maxvyseSkolkovne=17300;
    private final int sazbaPoplatnik=30840;
    private final int sazbaManzel=2070;
    private final int sazbaStudent=4020;
    private final int sazbaInvalidita12=2520;
    private final int sazbaInvalidita3=5040;
    private final int sazbaPrukazZTP=16140;
    private final double sazbaDan=0.15;
    private final int dite1=1267;
    private final int dite2=3127;
    private final int diteDalsi=2320;
    //sazby


    public DanovaKalkulacka() {
        this.prijmy = 0;
        this.vydaje = 0;
        this.pausal = 0;
        this.pocetDeti = 0;
        this.manzel = false;
        this.skolkovne = 0;
    }

    @Override
    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele) {
        this.individualniNastaveni = individualniNastaveniUzivatele;
        prijmy = individualniNastaveni.getIndPrijmy();
        vydaje = individualniNastaveni.getIndVydaje();
        pausal = individualniNastaveni.getIndPausal();
        pocetDeti = individualniNastaveni.getIndPocetDeti();
        penzijniPoj = individualniNastaveni.getIndPenzijniPoj();
        zivotPoj = individualniNastaveni.getIndZivotPoj();
        odpocetUroku = individualniNastaveni.getIndOdpocetUroku();
        invalidita12 = individualniNastaveni.isIndInvalidita12();
        invalidita3 = individualniNastaveni.isIndInvalidita3();
        prukazZTP = individualniNastaveni.isIndPrukazZTP();
        manzel = individualniNastaveni.isIndManzel();
        skolkovne = individualniNastaveni.getIndSkolkovne();
    }

    @Override
    public void vypocti() {

        if(isVydajove == false) {
            ZD = prijmy - (prijmy * pausal);
        }else {
            ZD = prijmy - vydaje;
        }

            if (ZD / 2 < celkoveSocialni) {
                soc = maxVyseSocialni;
            } else {
                soc = ZD / 2 * sazbaSocialni;
            }

            if (ZD/2 < celkoveZdravotni){
                zdrav = maxVyseZdravotni;
            }else {
                zdrav = ZD/2*sazbaZdravotni;
            }

            if(penzijniPoj-minVysePenzijni>=maxVysePenzijni){
                ZD -= maxVysePenzijni;
            }else if (penzijniPoj>minVysePenzijni) {
                ZD -= penzijniPoj-minVysePenzijni;
            }

            if(zivotPoj>=maxVyseZivotni){
                ZD -= maxVyseZivotni;
            }else if (zivotPoj>=minVyseZivotni) {
                ZD -= zivotPoj;
            }

            if (odpocetUroku>maxOdpocetUroku){
                ZD -= maxOdpocetUroku;
            }else {
                ZD -= odpocetUroku;
            }

            if (skolkovne>maxvyseSkolkovne){
                danSleva += maxvyseSkolkovne;
            }else {
                danSleva += skolkovne;
            }


        if (poplatnik==true){
            danSleva += sazbaPoplatnik;
        }
        if (manzel==true){
            danSleva += sazbaManzel;
        }
        if (student==true){
            danSleva += sazbaStudent;
        }
        if (invalidita12==true){
            danSleva += sazbaInvalidita12;
        }
        if (invalidita3==true){
            danSleva += sazbaInvalidita3;
        }
        if (prukazZTP==true){
            danSleva += sazbaPrukazZTP;
        }
        if (ZD-danSleva<0){
            danSleva = 0;
        }

        dan = zaokrouhliNaStovkyDolu(ZD)*sazbaDan;

        if(dan-danSleva<0){
            danPoSleva = 0;
        }else {
            danPoSleva = dan - danSleva;
        }

        if(pocetDeti == 0){
            danBonus = 0;
        } else if (pocetDeti == 1) {
            danBonus += dite1;
        } else if (pocetDeti == 2) {
            danBonus += dite2;
        } else if (pocetDeti > 2) {
            danBonus = danBonus + dite2 + (pocetDeti - 2)*diteDalsi;
        }

        danBonus = danPoSleva - danBonus;

    }

    public static double zaokrouhliNaStovkyDolu(double cislo) {
        return Math.floor(cislo / 100.0) * 100.0;
    }
}

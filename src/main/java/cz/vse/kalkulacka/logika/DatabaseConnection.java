package cz.vse.kalkulacka.logika;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseConnection {

    private static final String URL = "jdbc:mysql://localhost:3306/danovakalkulacka";
    private static final String USER = "kalkulacka";
    private static final String PASSWORD = "Kalkulačka2023";

    private Connection databazeConnection;

    public DatabaseConnection() {
        this.databazeConnection = null;
    }


    public Connection getConnection() {
        try {
            databazeConnection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Připojeno k databázi");
            return databazeConnection;
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Chyba při připojování k databázi: " + e.getMessage());
            return null;
        }
    }

    public void closeConnection(){
        if(this.databazeConnection != null){
            try{
                this.databazeConnection.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

//
//
//
//
//
//    // Metoda pro načtení uživatele z databáze
//    public RegistrovanyUzivatel nactiUzivatele(String xname, String heslo) {
//        String sql = "SELECT * FROM UZIVATEL WHERE xname = ? AND heslo = ?";
//
//        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
//             PreparedStatement statement = connection.prepareStatement(sql)) {
//            statement.setString(1, xname);
//            statement.setString(2, heslo);
//
//            try (ResultSet resultSet = statement.executeQuery()) {
//                if (resultSet.next()) {
//                    String jmeno = resultSet.getString("jmeno");
//                    String prijmeni = resultSet.getString("prijmeni");
//                    // další sloupce tabulky UZIVATEL
//
//                    return new RegistrovanyUzivatel(xname, jmeno, prijmeni, heslo);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
//
//    // Metoda pro načtení individuálního nastavení z databáze
//    public IndividualniNastaveni nactiIndividualniNastaveni(String xname) {
//        String sql = "SELECT * FROM INDIVIDUALNINASTAVENI WHERE xname = ?";
//
//        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
//             PreparedStatement statement = connection.prepareStatement(sql)) {
//            statement.setString(1, xname);
//
//            try (ResultSet resultSet = statement.executeQuery()) {
//                if (resultSet.next()) {
//                    boolean manzel = resultSet.getBoolean("manzel");
//                    boolean invalidita12 = resultSet.getBoolean("invalidita12");
//                    // další sloupce tabulky INDIVIDUALNINASTAVENI
//
//                    IndividualniNastaveni nastaveni = new IndividualniNastaveni();
//                    nastaveni.setManzel(manzel);
//                    nastaveni.setInvalidita12(invalidita12);
//                    // další nastavení
//
//                    return nastaveni;
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
}

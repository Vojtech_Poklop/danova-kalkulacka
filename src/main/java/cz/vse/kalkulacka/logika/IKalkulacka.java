package cz.vse.kalkulacka.logika;

public interface IKalkulacka {
    public void vypocti();

    public void nactiIndividualniAtributy(IndividualniNastaveni individualniNastaveniUzivatele);
}

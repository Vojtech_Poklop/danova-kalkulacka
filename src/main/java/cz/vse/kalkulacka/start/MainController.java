package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.Aplikace;
import cz.vse.kalkulacka.logika.IKalkulacka;
import cz.vse.kalkulacka.logika.KalkulackaFactory;
import cz.vse.kalkulacka.logika.Klient;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController {

    @FXML
    private VBox vysledky;
    @FXML
    private TextField registraceTextFieldXname;

    @FXML
    private TextField registraceTextFieldJmeno;

    @FXML
    private TextField registraceTextFieldPrijmeni;

    @FXML
    private PasswordField registraceTextFieldHeslo;

    @FXML
    private PasswordField registraceTextFieldHesloKontrola;

//    @FXML
//    public void popNoveStranky(MouseEvent mouseEvent) throws IOException {
//        System.out.println("Před vytvořením nového okna");
//
//        Stage stage = new Stage();
//        Parent root = FXMLLoader.load(getClass().getResource("VysOkVydajovaKalkulacka.fxml"));
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
//        stage.show();
//
//        System.out.println("Po vytvoření nového okna");
//    }



//    private final Aplikace aplikace = Klient.getAplikace();

    @FXML
    private PasswordField passFieldStareHeslo;

    @FXML
    private PasswordField passFieldNoveHeslo;

    @FXML
    private TextField uzivatelskeJmenoTextField;

    @FXML
    private PasswordField hesloTextField;

    @FXML
    private Label spatneUdajeLabel;

    @FXML
    private Label jmenoUzivateleLabel;

    @FXML
    private Label zmenaHlaskaLabel;

    @FXML
    private Label zmenaProfilHlaskaLabel;

    @FXML
    private TextField jmenoProfilField;

    @FXML
    private TextField prijmeniProfilField;
    private ActionEvent event;

    @FXML
    public void initialize(){
        if(Klient.getAplikace().isJePrihlasen()){
            jmenoUzivateleLabel.setText(Klient.getAplikace().getRegistrovanyUzivatel().getJmeno()+" "+Klient.getAplikace().getRegistrovanyUzivatel().getPrijmeni());
        }

        if(jmenoProfilField != null && prijmeniProfilField != null){
            jmenoProfilField.setText(Klient.getAplikace().getRegistrovanyUzivatel().getJmeno());
            prijmeniProfilField.setText(Klient.getAplikace().getRegistrovanyUzivatel().getPrijmeni());
        }

    }

    @FXML
    public void prihlasitSe(ActionEvent actionEvent){
        String vlozeneXname = uzivatelskeJmenoTextField.getText();
        System.out.println(vlozeneXname);
        String vlozeneHeslo = hesloTextField.getText();
        System.out.println("Kliknutí na přihlášení");
        if(Klient.getAplikace().prihlasitSe(vlozeneXname, vlozeneHeslo)){
            SceneManager.switchToSceneEvent(actionEvent);
        } else {
            spatneUdajeLabel.setVisible(true);
        }
    }

    @FXML
    public void registrovatSe(ActionEvent actionEvent){
        String vlozeneXname = registraceTextFieldXname.getText();
        String vlozeneJmeno = registraceTextFieldJmeno.getText();
        String vlozenePrijmeni = registraceTextFieldPrijmeni.getText();
        String vlozeneHeslo = registraceTextFieldHeslo.getText();
        String vlozeneHesloKontrola = registraceTextFieldHesloKontrola.getText();

        if(!vlozeneHeslo.equals(vlozeneHesloKontrola)){
            System.out.println("Zadaná hesla nejsou stejná!");
            spatneUdajeLabel.setVisible(true);
            spatneUdajeLabel.setText("Hesla se neshodují!");
            return;
        }

        if(Klient.getAplikace().registrovatSe(vlozeneXname,vlozeneJmeno,vlozenePrijmeni, vlozeneHeslo)){
            SceneManager.switchToSceneEvent(actionEvent);
        }else {
            spatneUdajeLabel.setVisible(true);
            spatneUdajeLabel.setText("Chybějící nevyplněné atributy!");
        }
    }

    @FXML
    public void odhlasitSe(MouseEvent mouseEvent) {
        Klient.getAplikace().odhlasitSe();
        SceneManager.switchToSceneEventMouse(mouseEvent);
    }

    @FXML
    public void zmenaStranky(ActionEvent actionEvent){
        System.out.println("Před načtením FXML");
        SceneManager.switchToSceneEvent(actionEvent);
        System.out.println("Po načtení FXML");
    }

    @FXML
    public void zmenaStrankyMouse(MouseEvent mouseEvent){
        System.out.println("Před načtením FXML");
        SceneManager.switchToSceneEventMouse(mouseEvent);
        System.out.println("Po načtení FXML");
    }

    @FXML
    private void zmenaHesla(ActionEvent actionEvent){
        String stareHeslo = passFieldStareHeslo.getText();
        String noveHeslo = passFieldNoveHeslo.getText();
        if(Klient.getAplikace().zmenaHesla(stareHeslo,noveHeslo)){
            zmenaHlaskaLabel.setVisible(true);
        }
        System.out.println("Heslo změněno na: "+noveHeslo);
    }

    @FXML
    protected void otevritKalkulacku(MouseEvent mouseEvent){

        String fxId;
        if(mouseEvent.getSource() instanceof VBox){
            VBox vbox = (VBox) mouseEvent.getSource();
            fxId = vbox.getId();
        }else{
            fxId = ((Button) mouseEvent.getSource()).getId();
        }
        String nazevKalkulacky = Character.toUpperCase(fxId.charAt(0)) + fxId.substring(1);

        KalkulackaFactory kalkulackaFactory = new KalkulackaFactory();
        IKalkulacka kalkulacka = kalkulackaFactory.vytvorKalkulacku(nazevKalkulacky);

        if(kalkulacka != null){
            System.out.println("Otevírám kalkulačku");
            Klient.getAplikace().otevriKalkulacku(kalkulacka);
            zmenaStrankyMouse(mouseEvent);
        } else {
            System.out.println("Neznámý typ kalkulačky: "+nazevKalkulacky);
        }
    }

    @FXML
    protected void zmenaJmenaPrijmeni(ActionEvent actionEvent){

        String noveJmeno = jmenoProfilField.getText();
        String novePrijmeni = prijmeniProfilField.getText();

        if(Klient.getAplikace().zmenaJmenaPrijmeniUzivatele(noveJmeno,novePrijmeni)){
            zmenaProfilHlaskaLabel.setVisible(true);
            initialize();
        }
    }
    public void popNoveStranky(MouseEvent mouseEvent) {
/*        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("VysOkVydajovaKalkulacka.fxml"));
            Parent root = loader.load();

            Stage stage = new Stage();
            stage.setTitle("Nové Okno");
            stage.setScene(new Scene(root));

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @FXML
    protected void zobrazeniVysledku(ActionEvent event) {
        vysledky.setVisible(true);
    }

    public void mackniEnter(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            prihlasitSe(event);
        }
    }

}


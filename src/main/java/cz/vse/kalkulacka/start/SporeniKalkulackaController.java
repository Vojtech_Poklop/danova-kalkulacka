package cz.vse.kalkulacka.start;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class SporeniKalkulackaController extends MainController{

    @FXML
    private TextField pocatecniVklad;
    @FXML
    private TextField mesicniUlozka;
    @FXML
    private TextField rocniUrok;
    @FXML
    private Spinner danZUroku;
    @FXML
    private Button spocitat;

   }

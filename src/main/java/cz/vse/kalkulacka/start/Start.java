package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.Aplikace;
import cz.vse.kalkulacka.logika.Klient;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Start extends Application {
    public static void main(String[] args) {
        System.out.println();
        launch(args);
        System.out.println("Jsem zde");

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/vse/kalkulacka/PrihlasovaciStranka.fxml"));
            FXMLLoader loader = new FXMLLoader();

            // Vytvoření instance Aplikace
            Aplikace aplikace = new Aplikace();
            Klient klient = new Klient();
            Klient.inicializovat(aplikace);
//            SceneManager sceneManager = new SceneManager(aplikace);

            // Předání instance Aplikace do konstruktoru MainControlleru
//            loader.setController(new MainController(aplikace));
            loader.setLocation(getClass().getResource("/cz/vse/kalkulacka/PrihlasovaciStranka.fxml"));


            Scene scene = new Scene(loader.load(), 1280,720);

            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setTitle("Daňová kalkulačka");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.*;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class SceneManager {
    public static void switchToSceneEvent(ActionEvent actionEvent){
        performSwitch(actionEvent);
    }

    public static void switchToSceneEventMouse(MouseEvent mouseEvent){
        performSwitch(mouseEvent);
    }


    public static void performSwitch(Event event){
        try {
//            String fxmlPath = "/cz/vse/kalkulacka/" +fxmlFileName +".fxml";
            String fxmlPath = getIdAndGenerateFxmlFileName(event);
            System.out.println(fxmlPath);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SceneManager.class.getResource(fxmlPath));

            Parent root = loader.load();
            Scene scene = new Scene(root, 1280,720);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(getIdAndGenerateFxmlFileName(event));
            System.out.println("Chyba při přechodu na jinou obrazovku!");
        }
    }

    private static String getIdAndGenerateFxmlFileName(Event event) {
        if (event.getSource() instanceof Node) {
            return generateFxmlFileName(getIdName(event));
        }
        System.out.println("Není Node instance");
        return "";
    }

    private static String generateFxmlFileName(String nodeId) {
        return "/cz/vse/kalkulacka/"+Character.toUpperCase(nodeId.charAt(0)) + nodeId.substring(1) + ".fxml";
    }

    private static String getIdName(Event event){
        if (event.getSource() instanceof Node) {
            Node sourceNode = (Node) event.getSource();
            String nodeId = sourceNode.getId();
            return nodeId;
        }else {
            return "";
        }
    }

    }


package cz.vse.kalkulacka.start;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class HypotecniKalkulackaController extends MainController{

    @FXML
    private TextField hypotecniPujcka;
    @FXML
    private TextField rocniUrok;
    @FXML
    private TextField dobaSplaceniHypoteky;
    @FXML
    private Button spocitat;

}

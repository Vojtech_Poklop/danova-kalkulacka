package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.IndividualniNastaveni;
import cz.vse.kalkulacka.logika.Klient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

public class DanovaKalkulackaController extends MainController{

    @FXML
    private TextField prijmyField;
    @FXML
    private TextField vydajeField;
    @FXML
    private ChoiceBox pausalChoiceBox;
    @FXML
    private Spinner pocetDetiSpinner;
    @FXML
    private TextField penzijniPojisteniField;
    @FXML
    private TextField zivotniPojisteniField;
    @FXML
    private TextField odpocetUrokuField;
    @FXML
    private TextField skolkovneField;
    @FXML
    private CheckBox poplatnikBox;
    @FXML
    private CheckBox invalida12Box;
    @FXML
    private CheckBox invalida3Box;
    @FXML
    private CheckBox prukazZTPBox;
    @FXML
    private CheckBox slevaManzelBox;
    @FXML
    private CheckBox studentBox;
    @FXML
    private Button spocitat;


    private SpinnerValueFactory<Integer> pocetDetiValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 15, 0);

    private ObservableList<Double> pausalValueOptions = FXCollections.observableArrayList(0.30, 0.40, 0.60, 0.80);


    @Override
    public void initialize() {
        super.initialize();
        nactiPocatecniHodnoty();
        pausalChoiceBox.setItems(pausalValueOptions);

    }


    private void nactiPocatecniHodnoty(){
        IndividualniNastaveni individualniNastaveni;
        if(Klient.getAplikace().isJePrihlasen()){
            individualniNastaveni = Klient.getAplikace().getRegistrovanyUzivatel().getIndividualniNastaveni();
        } else {
            individualniNastaveni = Klient.getAplikace().getDefaultniIndividualniNastaveni();
        }

        pocetDetiSpinner.setValueFactory(pocetDetiValueFactory);
        pocetDetiSpinner.getValueFactory().setValue(individualniNastaveni.getIndPocetDeti());


        pausalChoiceBox.setValue(individualniNastaveni.getIndPausal());

        penzijniPojisteniField.setText(String.valueOf(individualniNastaveni.getIndPenzijniPoj()));
        zivotniPojisteniField.setText(String.valueOf(individualniNastaveni.getIndZivotPoj()));
        odpocetUrokuField.setText(String.valueOf(individualniNastaveni.getIndOdpocetUroku()));
        skolkovneField.setText(String.valueOf(individualniNastaveni.getIndSkolkovne()));

        // Nastavení hodnot do CheckBoxů
        invalida12Box.setSelected(individualniNastaveni.isIndInvalidita12());
        invalida3Box.setSelected(individualniNastaveni.isIndInvalidita3());
        prukazZTPBox.setSelected(individualniNastaveni.isIndPrukazZTP());
        slevaManzelBox.setSelected(individualniNastaveni.isIndManzel());





    }


//    @FXML
//    protected void zmenaStrankyMouse(MouseEvent mouseEvent) {
//        mainController.zmenaStrankyMouse(mouseEvent);
//    }
//    @FXML
//    protected void otevritKalkulacku(MouseEvent mouseEvent) {
//        mainController.otevritKalkulacku(mouseEvent);
//    }


}

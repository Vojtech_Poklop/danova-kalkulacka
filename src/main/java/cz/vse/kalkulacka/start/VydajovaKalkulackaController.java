package cz.vse.kalkulacka.start;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.io.IOException;

public class VydajovaKalkulackaController extends MainController{

    @FXML
    private Button spocitat;
    @FXML
    private DatePicker odRozmezi;
    @FXML
    private DatePicker doRozmezi;


    @FXML
    private TextField VysNazev;
    @FXML
    private DatePicker VysDatumPlatby;
    @FXML
    private Spinner VysOpakovat;
    @FXML
    private TextField VysCastka;
    @FXML
    private TextArea VysPoznamka;
    @FXML
    private Button VysUlozit;



}

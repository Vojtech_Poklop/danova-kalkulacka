package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.IndividualniNastaveni;
import cz.vse.kalkulacka.logika.Klient;
import cz.vse.kalkulacka.logika.MzdovaKalkulacka;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class MzdovaKalkulackaController extends MainController {


    @FXML
    private TextField hrubaMzdaField;
    @FXML
    private Spinner pocetDetiSpinner;
    @FXML
    private TextField cenaSluzebnihoVozuField;
    @FXML
    private ChoiceBox<String> druhSluzebnihoVozuChoiceBox;
    @FXML
    private CheckBox invalida12Box;
    @FXML
    private CheckBox invalida3Box;
    @FXML
    private CheckBox prukazZTPBox;

    @FXML
    private TextField  cistaMzdaField;

    @FXML
    private TextField zakladVypocetZalohyField;

    @FXML
    private TextField danPredSlevamiField;

    @FXML
    private TextField slevyNaDaniField;

    @FXML
    private TextField danField;

    @FXML
    private TextField danZvNaDetField;

    @FXML
    private TextField danBonusField;

    @FXML
    private TextField socAZdravZamestnavatelField;

    @FXML
    private TextField socAZdravZamestnanecLabel;

    @FXML
    private TextField odvodyField;

    private SpinnerValueFactory<Integer> pocetDetiValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 15, 0);
    private ObservableList<String> druhyVozuOptions = FXCollections.observableArrayList("Běžné", "Nízkoemisní", "Bezemisní");


    @Override
    public void initialize() {
        super.initialize();
        nactiPocatecniHodnoty();
        druhSluzebnihoVozuChoiceBox.setItems(druhyVozuOptions);

    }


    private void nactiPocatecniHodnoty(){
        IndividualniNastaveni individualniNastaveni;
        if(Klient.getAplikace().isJePrihlasen()){
            individualniNastaveni = Klient.getAplikace().getRegistrovanyUzivatel().getIndividualniNastaveni();
        } else {
            individualniNastaveni = Klient.getAplikace().getDefaultniIndividualniNastaveni();
        }

        pocetDetiSpinner.setValueFactory(pocetDetiValueFactory);
        pocetDetiSpinner.getValueFactory().setValue(individualniNastaveni.getIndPocetDeti());

        druhSluzebnihoVozuChoiceBox.setValue(individualniNastaveni.getIndTypAuto());


        cenaSluzebnihoVozuField.setText(String.valueOf(individualniNastaveni.getIndCenaSluzebnihoVozu()));
        hrubaMzdaField.setText(String.valueOf(individualniNastaveni.getIndHrubaMzda()));

        // Nastavení hodnot do CheckBoxů
        invalida12Box.setSelected(individualniNastaveni.isIndInvalidita12());
        invalida3Box.setSelected(individualniNastaveni.isIndInvalidita3());
        prukazZTPBox.setSelected(individualniNastaveni.isIndPrukazZTP());
    }

    private void spocitat() {
        // Vytvoření instance MzdovaKalkulacka
        MzdovaKalkulacka mzdovaKalkulacka = (MzdovaKalkulacka) Klient.getAplikace().getKalkulacka();

        // Nastavení atributů z GUI do instance MzdovaKalkulacka
        mzdovaKalkulacka.setHrubaMzda(Integer.parseInt(hrubaMzdaField.getText()));
        mzdovaKalkulacka.setPocetDeti((Integer) pocetDetiSpinner.getValue());
        mzdovaKalkulacka.setCenaSluzebnihoVozu(Integer.parseInt(cenaSluzebnihoVozuField.getText()));
        mzdovaKalkulacka.setInvalidita12(invalida12Box.isSelected());
        mzdovaKalkulacka.setInvalidita3(invalida3Box.isSelected());
        mzdovaKalkulacka.setPrukazZTP(prukazZTPBox.isSelected());
        mzdovaKalkulacka.setTypAuto(druhSluzebnihoVozuChoiceBox.getValue());

        // Volání metody vypocti
        mzdovaKalkulacka.vypocti();

// Získání vypočtených hodnot
        int cistaMzda = (int) mzdovaKalkulacka.getCistaMzda();
        int zakladVypocetZalohy = (int) mzdovaKalkulacka.getZD();
        int danPredSlevami = (int) mzdovaKalkulacka.getDanPredSlevami();
        int slevyNaDani = (int) mzdovaKalkulacka.getSlevyNaDani();
        int dan = (int) mzdovaKalkulacka.getDan();
        int danZvNaDet = (int) mzdovaKalkulacka.getZvyhodneniDeti();
        int danBonus = (int) mzdovaKalkulacka.getDanovyBonusCelkem();
        int socAZdravZamestnavatel = (int) mzdovaKalkulacka.getSocZdravZamestnavatel();
        int socAZdravZamestnanec = (int) mzdovaKalkulacka.getSocZdravZamestnanec();
        int odvody = (int) mzdovaKalkulacka.getOdvody();

        // Nastavení hodnot do TextFieldů
        cistaMzdaField.setText(String.valueOf(cistaMzda)+" Kč");
        zakladVypocetZalohyField.setText(String.valueOf(zakladVypocetZalohy)+" Kč");
        danPredSlevamiField.setText(String.valueOf(danPredSlevami)+" Kč");
        slevyNaDaniField.setText(String.valueOf(slevyNaDani)+" Kč");
        danField.setText(String.valueOf(dan)+" Kč");
        danZvNaDetField.setText(String.valueOf(danZvNaDet)+" Kč");
        danBonusField.setText(String.valueOf(danBonus)+" Kč");
        socAZdravZamestnavatelField.setText(String.valueOf(socAZdravZamestnanec)+" Kč");
        socAZdravZamestnanecLabel.setText(String.valueOf(socAZdravZamestnavatel)+" Kč");
        odvodyField.setText(String.valueOf(odvody)+" Kč");

    }

    @Override
    protected void zobrazeniVysledku(ActionEvent event) {
        super.zobrazeniVysledku(event);
        spocitat();
    }
}

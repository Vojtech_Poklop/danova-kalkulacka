package cz.vse.kalkulacka.start;

import cz.vse.kalkulacka.logika.IndividualniNastaveni;
import cz.vse.kalkulacka.logika.Klient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class IndividualniNastaveniController extends MainController {

    @FXML
    private Label nastaveniUlozeno;
    @FXML
    private TextField hrubaMzdaField;
    @FXML
    private Spinner<Integer> pocetDetiSpinner;
    @FXML
    private TextField sluzebniAutoField;
    @FXML
    private ChoiceBox<String> druhSluzebnihoVozuChoiceBox;
    @FXML
    private CheckBox invalida12Box;
    @FXML
    private CheckBox invalida3Box;
    @FXML
    private CheckBox prukazZTPBox;
    @FXML
    private TextField rocniPrijmyField;
    @FXML
    private TextField rocniVydajeField;
    @FXML
    private ChoiceBox<Double> pausalChoiceBox;
    @FXML
    private CheckBox slevaManzelBox;
    @FXML
    private TextField penzijniPojisteniField;
    @FXML
    private TextField zivotniPojisteniField;
    @FXML
    private TextField odpocetUrokuField;
    @FXML
    private TextField skolkovneField;
    @FXML
    private TextField prumernaMzdaCtvrtletiField;
    @FXML
    private TextField prumernaMzdaRocniField;

    private SpinnerValueFactory<Integer> pocetDetiValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 15, 0);

    private ObservableList<Double> pausalValueOptions = FXCollections.observableArrayList(0.30, 0.40, 0.60, 0.80);
    private ObservableList<String> druhyVozuOptions = FXCollections.observableArrayList("Běžné", "Nízkoemisní", "Bezemisní");



    @Override
    public void initialize() {
        super.initialize();
        nactiPocatecniHodnoty();
        druhSluzebnihoVozuChoiceBox.setItems(druhyVozuOptions);
        pausalChoiceBox.setItems(pausalValueOptions);

    }


    private void nactiPocatecniHodnoty(){
        IndividualniNastaveni individualniNastaveni;
        if(Klient.getAplikace().isJePrihlasen()){
            individualniNastaveni = Klient.getAplikace().getRegistrovanyUzivatel().getIndividualniNastaveni();
        } else {
            individualniNastaveni = Klient.getAplikace().getDefaultniIndividualniNastaveni();
        }

        pocetDetiSpinner.setValueFactory(pocetDetiValueFactory);
        pocetDetiSpinner.getValueFactory().setValue(individualniNastaveni.getIndPocetDeti());


        pausalChoiceBox.setValue(individualniNastaveni.getIndPausal());
        druhSluzebnihoVozuChoiceBox.setValue(individualniNastaveni.getIndTypAuto());



        sluzebniAutoField.setText(String.valueOf(individualniNastaveni.getIndCenaSluzebnihoVozu()));
        hrubaMzdaField.setText(String.valueOf(individualniNastaveni.getIndHrubaMzda()));
        rocniPrijmyField.setText(String.valueOf(individualniNastaveni.getIndPrijmy()));
        rocniVydajeField.setText(String.valueOf(individualniNastaveni.getIndVydaje()));
        penzijniPojisteniField.setText(String.valueOf(individualniNastaveni.getIndPenzijniPoj()));
        zivotniPojisteniField.setText(String.valueOf(individualniNastaveni.getIndZivotPoj()));
        odpocetUrokuField.setText(String.valueOf(individualniNastaveni.getIndOdpocetUroku()));
        skolkovneField.setText(String.valueOf(individualniNastaveni.getIndSkolkovne()));
        prumernaMzdaCtvrtletiField.setText(String.valueOf(individualniNastaveni.getIndPrumMzda4()));
        prumernaMzdaRocniField.setText(String.valueOf(individualniNastaveni.getIndPrumMzda12()));

        // Nastavení hodnot do CheckBoxů
        invalida12Box.setSelected(individualniNastaveni.isIndInvalidita12());
        invalida3Box.setSelected(individualniNastaveni.isIndInvalidita3());
        prukazZTPBox.setSelected(individualniNastaveni.isIndPrukazZTP());
        slevaManzelBox.setSelected(individualniNastaveni.isIndManzel());
    }


    @FXML
    private void ulozAktualniNastaveni() {
        IndividualniNastaveni individualniNastaveni;

        if (Klient.getAplikace().isJePrihlasen()) {
            individualniNastaveni = Klient.getAplikace().getRegistrovanyUzivatel().getIndividualniNastaveni();
        } else {
            individualniNastaveni = Klient.getAplikace().getDefaultniIndividualniNastaveni();
        }

        // Nastavení hodnoty pro Spinner
        individualniNastaveni.setIndPocetDeti(pocetDetiSpinner.getValue());

        // Nastavení hodnoty pro ChoiceBox
        individualniNastaveni.setIndTypAuto(druhSluzebnihoVozuChoiceBox.getValue());
        individualniNastaveni.setIndPausal(pausalChoiceBox.getValue());

        // Nastavení hodnoty pro ostatní ovládací prvky
        individualniNastaveni.setIndHrubaMzda(Integer.parseInt(hrubaMzdaField.getText()));
        individualniNastaveni.setIndPrijmy(Integer.parseInt(rocniPrijmyField.getText()));
        individualniNastaveni.setIndVydaje(Integer.parseInt(rocniVydajeField.getText()));
        individualniNastaveni.setIndCenaSluzebnihoVozu(Integer.parseInt(sluzebniAutoField.getText()));
        individualniNastaveni.setIndManzel(slevaManzelBox.isSelected());
        individualniNastaveni.setIndPenzijniPoj(Integer.parseInt(penzijniPojisteniField.getText()));
        individualniNastaveni.setIndZivotPoj(Integer.parseInt(zivotniPojisteniField.getText()));
        individualniNastaveni.setIndOdpocetUroku(Integer.parseInt(odpocetUrokuField.getText()));
        individualniNastaveni.setIndSkolkovne(Integer.parseInt(skolkovneField.getText()));
        individualniNastaveni.setIndPrumMzda4(Integer.parseInt(prumernaMzdaCtvrtletiField.getText()));
        individualniNastaveni.setIndPrumMzda12(Integer.parseInt(prumernaMzdaRocniField.getText()));

        // Nastavení hodnot do CheckBoxů
        individualniNastaveni.setIndInvalidita12(invalida12Box.isSelected());
        individualniNastaveni.setIndInvalidita3(invalida3Box.isSelected());
        individualniNastaveni.setIndPrukazZTP(prukazZTPBox.isSelected());

        nastaveniUlozeno.setVisible(true);

        // Uložení do instance Klienta
        if (Klient.getAplikace().isJePrihlasen()) {
            Klient.getAplikace().getRegistrovanyUzivatel().setIndividualniNastaveni(individualniNastaveni);
            Klient.getAplikace().ulozNastaveniDoDatabaze(Klient.getAplikace().getRegistrovanyUzivatel().getXname(), individualniNastaveni);
        } else {
            System.out.println("Klient není příhlášený! Nic se neuloží!");
        }
    }



}

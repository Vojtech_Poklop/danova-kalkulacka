package cz.vse.kalkulacka.start;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class NemocenskaKalkulackaController extends MainController{

    @FXML
    private TextField prumMzda12;
    @FXML
    private TextField prumMzda4;
    @FXML
    private DatePicker zacatekNemoci;
    @FXML
    private DatePicker konecNemoci;
    @FXML
    private Button spocitat;

}
